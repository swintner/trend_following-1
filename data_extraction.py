import os
from zipfile import ZipFile
import wget
from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import URLError, HTTPError
import ftplib
from datetime import date, timedelta
import pandas as pd


def scraping_links_from_urls(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        print(e)
    except URLError as e:
        url = input('Please check the url and re-enter')
        html = urlopen(url)

    bsObj = BeautifulSoup(html.read(), features="html.parser")
    links = []
    for link in bsObj.findAll('a'):
        link = link.get('href')
        if link not in links:
            links.append(link)

    return links

#  how do I schedule this to run at XX:XX:XX EST
#  need to implement user choice fo file to search for and download; possibly as a second, new function and preserve get_cme_settle as a scheduled/
#  automated task
#  need to mod to allow user to designate download directory
#  how do I use OS to get current user profile name and automatically save to desktop if download_path is not specified
def get_cme_settle(ftp_url='ftp.cmegroup.com', ftp_cwd='pub/settle', download_path='ftp_cme_downloads/'):
    ftp = ftplib.FTP(ftp_url)
    ftp.login()
    ftp.cwd(ftp_cwd)
    now = date.today()
    yesterday = now - timedelta(1)
    already_downloaded = pd.read_csv(f'all_files_cme_{yesterday}', index_col=0, names=['files'])
    already_downloaded = already_downloaded['files'].to_list()
    todays_files = ftp.nlst()
    files_to_download = [i for i in todays_files if i not in already_downloaded]
    for i in files_to_download:
        wget.download(f'{ftp_url}/{ftp_cwd}/{i}', download_path)
        if i.endswith('.zip'):
            ZipFile(f'{i}', 'r').extractall(download_path)
        already_downloaded = already_downloaded.append(i)
    expiring_options = [i for i in todays_files if 'Expiring_Options_' in i] # <-- probably not needed
    cbt_settles_e = [i for i in todays_files if 'cbt.settle' in i and i.endswith('e.csv')]  # <-- probably not needed
    cbt_settles_s = [i for i in todays_files if 'cbt.settle' in i and i.endswith('s.csv')]  # <-- probably not needed
    cme_settles_e = [i for i in todays_files if 'cme.settle' in i and i.endswith('e.csv')]  # <-- probably not needed
    cme_settles_s = [i for i in todays_files if 'cme.settle' in i and i.endswith('s.csv')]  # <-- probably not needed

    # save new already_downloaded list to file
    pd.DataFrame(already_downloaded).to_csv(f'all_files_cme_{now}')
    # delete old already_downloaded list

    # close ftp
    ftp.close()


def get_and_unzip(url_of_file, destination_path, zipped_file_path):
    wget.download(url_of_file, destination_path)
    ZipFile(zipped_file_path, 'r').extractall(destination_path)
    return




# textbook url 'http://www.pythonscraping.com/pages/page1.html'
# ftp://ftp.cmegroup.com/pub/settle/Expiring_Options_UnderlyingProduct_SettlementReport.csv
# ftp://ftp.cmegroup.com/pub/settle/cbt.settle.20190906.s.txt

