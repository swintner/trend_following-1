import random
import numpy as np
import pandas as pd  # use np to access numpy
import scipy.stats as sc_stats  # needed for norm.cdf


def make_year():
    return pd.DataFrame(pd.date_range(start='1/1/19', end='12/31/19'), columns=['dateOnly'])


def _check_if_data_frame(input_data):
    """
    :type input_data: DataFrame
    """
    #  check for dataframe
    if not isinstance(input_data, pd.DataFrame):
        _make_data_frame(input_data)
    elif isinstance(input_data, pd.DataFrame):
        _check_fields(input_data)
    return


def _make_data_frame(input_data):
    """
    :type input_data: DataFrame
    """
    if not isinstance(input_data, pd.DataFrame):
        # raise Exception('only dataframes please', type(input_data))
        input_data = pd.DataFrame(input_data)
    return input_data


def _check_fields(input_data):

    list_of_fields = ['spx', 'es', 'expiry', 'date', 'market']

    #  get data column names
    data_fields = input_data.columns
    if len(data_fields) == 0:
        print('There are no data headers')

    missing = [i for i in list_of_fields if i not in data_fields]
    for i in missing:
        print('{0} is missing'.format(i))

    return


def time_missing_filter(input_data, date_field='date'):
    """
    :param date_field: is the current calendar date
    :return: returns a filter for missing time in date only element to prep for dt conversion
    :type input_data: DataFrame
    """
    time_missing = (input_data[date_field].dt.hour == 0) & \
                   (input_data[date_field].dt.minute == 0) & \
                   (input_data[date_field].dt.second == 0)
    return time_missing


def make_dates(input_data, year_field='year', month_field='month', day_field='day'):

    #  create the date format columns
    mod_date = pd.to_datetime(dict(year=input_data[year_field], month=input_data[month_field], day=input_data[day_field]))
    return mod_date


def get_eod_times(input_data, date_field='date', market_field='spx'):
    """
    :type market_field: str
    :param date_field: is the current calendar date
    :param market_field: must be SPX or ES, lowercased
    :return: corrected end of day time
    :type input_data: DataFrame
    """

    print('\nGet EOD times dataframe check:')
    _check_if_data_frame(input_data)
    time_missing = time_missing_filter(input_data, date_field=date_field)

    if np.isin(market_field.lower(), ['spx', 'es']):
        eod_time = np.timedelta64(16 * 60 + 15, 'm')
    elif np.isin(market_field.lower(), ['dax']):
        eod_time = np.timedelta64(11 * 60 + 30, 'm')
    else:
        eod_time = np.timedelta64(0, 'm')
        print('market_field {0} unknown'.format(market_field))
    return input_data[date_field] + time_missing * eod_time


#  gets


def get_market_expiry_times(input_data, expiry_field='expiry', market_field='spx'):
    """
    :type market_field: string or list of markets
    :type expiry_field: series
    :param expiry_field: the expiration date field of the dataframe
    :param market_field: must be SPX or ES, lowercased
    :return: expiry date and expiry time as single datetime column
    :type input_data: DateFrame
    """
    print('\nGet market expiry times dataframe check:')
    _check_if_data_frame(input_data)
    #  when using datetime.[year, month, day] Friday == 4; Saturday == 5
    expiries = input_data[expiry_field] - (np.timedelta64(1, 'D') * (input_data[expiry_field].dt.weekday == 5)).values

    #  check if market_field is SPX or ES; established in this format to allow for future use of other markets
    if np.isin(market_field.lower(), ['spx', 'es']):
        #  third friday,  determined by constraint of third friday must be 14 > 22
        third_friday_filter = (expiries.dt.day > 14) & (expiries.dt.day < 22) & (expiries.dt.weekday == 4)

        #  need to check if quarterly third friday was successful
        #  must create separate columns for date and time and then use np.where(market_field == thirdfriday, 1, 0)
        if market_field.lower() == 'es':
            third_friday_filter = third_friday_filter & expiries.dt.month.isin([3, 6, 9, 12])

        #  8 * 60 + 30 == 0830EST;  16 * 60 == 1600EST
        expiry_times = third_friday_filter * np.timedelta64(9 * 60 + 30, 'm') + (~third_friday_filter) * np.timedelta64(16 * 60, 'm')
    else:
        expiry_times = np.timedelta64(0, 'm')
        print('market_field {0} unknown'.format(market_field))
    return expiries + expiry_times


def get_option_lifetimes(input_data, market_field='spx', time_delta_unit='Y', date_field='date', expiry_field='expiry'):
    """
    :param market_field: SPX or ES
    :param time_delta_unit: must be in list below
    :param date_field: the current calendar date
    :param expiry_field: the expiration date of the option0
    :type input_data: DataFrame
    """

    print('\nGet option lifetimes dataframe check:')
    _check_if_data_frame(input_data)

    # check expiries
    expiries = get_market_expiry_times(input_data, expiry_field=expiry_field, market_field=market_field)

    # check time
    valuation_date_time = get_eod_times(input_data, date_field=date_field, market_field=market_field)

    # calculate lifetime in given units
    lifetime = (expiries - valuation_date_time) / np.timedelta64(1, time_delta_unit)
    return lifetime

#  CALCULATIONS


def single_option_price(call_put=1, forward=100, strike=100, risk_free=0, lifetime=1, volatility=0.15):
    """
    Apply Black Scholes Merton on a 1-d slice along the given axis

    :param call_put:
    :param forward:
    :param strike:
    :param risk_free:
    :param lifetime:
    :param volatility:

    :returns  A single option price
    """

    d1 = (np.log(forward / strike) + np.power(volatility, 2) * lifetime) / (volatility * np.sqrt(lifetime))
    d2 = d1 - volatility * np.sqrt(lifetime)
    the_price = 0  # set to zero to avoid 'referenced before assignment error'
    if call_put == 1:
        the_price = forward * sc_stats.norm.cdf(d1) - strike * sc_stats.norm.cdf(d2)
    elif call_put == -1:
        the_price = strike * sc_stats.norm.cdf(-d2) - forward * sc_stats.norm.cdf(-d1)

    the_price = np.exp(-risk_free * lifetime) * the_price

    return the_price


def multiple_prices(input_data, call_put_field='call_put', forward_field='forward', strike_field='strike', lifetime_field='lifetime',
                    risk_free_field='risk_free', vol_field='volatility'):
    """
    Apply Black Scholes Merton on a 1-d slice along the given axis.

    Can only be used on dataframes with complete information

    :returns  mutltiple prices
    """
    #  A KEY ERROR IS OCCURING HERE at d1 at forward, according to frame.py(source file)
    d1 = (np.log(input_data[forward_field] / input_data[strike_field]) + 0.5*np.power(input_data[vol_field], 2) *
          input_data[lifetime_field]) / (input_data[vol_field] * np.sqrt(input_data[lifetime_field]))
    d2 = d1 - input_data[vol_field] * np.sqrt(input_data[lifetime_field])

    #  ERROR EXISTS HERE operands could not be broadcast together with shapes (35,) (0,)

    return (input_data[call_put_field] * input_data[forward_field] * sc_stats.norm.cdf(input_data[call_put_field] * d1) -
        input_data[call_put_field]*sc_stats.norm.cdf(input_data[call_put_field] * d2) * input_data[strike_field]) * \
                           np.exp(-input_data[risk_free_field] * input_data[lifetime_field])


#   KEEP


def implied_volatility(input_data, call_put_field='call_put', price_field='price', forward_field='forward', strike_field='strike', lifetime_field='lifetime',
                    risk_free_field='risk_free', vol_field='vol', max_iter=1e5, tolerance=1e-6, vol_delta=0.001):

    px = multiple_prices(input_data, call_put_field=call_put_field, forward_field=forward_field, strike_field=strike_field,
                         lifetime_field=lifetime_field, risk_free_field=risk_free_field, vol_field=vol_field)

    # px.drop(columns=)
    #  the expression above returns an entire dataframe, causing the below mod to be needed
    price_filter1 = (px['call_prices'] < input_data[price_field] / 2) & (px['put_prices'] < input_data[price_field] / 2)
    input_data.loc[price_filter1, vol_field] = input_data.loc[price_filter1, vol_field] * 2

    # px is a single array so it can be sliced on the filter
    px[price_filter1] = multiple_prices(input_data.loc[price_filter1], call_put_field=call_put_field, forward_field=forward_field,
                                           strike_field=strike_field, lifetime_field=lifetime_field, risk_free_field=risk_free_field, vol_field=vol_field)

    price_filter2 = ((px['call_prices'] - input_data[price_field]).abs() > tolerance) & ((px['put_prices'] - input_data[price_field]).abs() > tolerance)

    for i in range(1, int(max_iter)):
        start_px = multiple_prices(input_data.loc[price_filter2], call_put_field=call_put_field,  forward_field=forward_field,
                                        strike_field=strike_field, lifetime_field=lifetime_field, risk_free_field=risk_free_field, vol_field=vol_field)
        start_dev = input_data[price_field] - start_px
        price_filter2.loc[start_dev.abs() < tolerance] = False

        #  break filter 3
        origVol = input_data[vol_field].copy()
        input_data[vol_field] = input_data[vol_field] - vol_delta
        price_filter2.loc[input_data[vol_field] < 0] = False

        px = multiple_prices(input_data.loc[price_filter2], call_put_field=call_put_field,  forward_field=forward_field,
                             strike_field=strike_field, lifetime_field=lifetime_field, risk_free_field=risk_free_field, vol_field=vol_field)
        vol_delta_dev = input_data[price_field] - px
        end_dev = (start_dev - vol_delta_dev) / vol_delta
        price_filter2.loc[abs(end_dev) < tolerance] = False
        input_data[vol_field] = origVol - (start_dev / end_dev)

    return input_data[vol_field]


# def iv_range(call_put=1, price=100, forward=100, strike=100, lifetime=1, risk_free=0, starting_vol=0.15, max_iter=1e5, tolerance=1e-6):
#     the_iv = np.ones((len(call_put),)) * np.nan
#     for rr in range(len(call_put)):
#         the_iv = implied_volatility(call_put[rr], price[rr], forward[rr], strike[rr], lifetime[rr], risk_free[rr],
#                                     starting_vol, max_iter, tolerance)
#     return the_iv

#  CALCULATE EXPIRY VALUES


def calc_expiry_values(options, risk_free, date_field='date', expiry_field='expiry', market='spx', id_field='oid'):

    return


#  TEST DRIVERS  #
def gen_list(size):
    # initialize random list with values between 0 and 100
    random_list = []
    for i in range(size):
        random_list.append(random.randint(0, 10))
    return random_list


def sum_list(in_list):
    # return the sum of all elements in the list
    # This is the same as "return sum(inList)" but in long form for readability and emphasis
    final_sum = 0

    # iterate over all values in the list, and calculate the cummulative sum
    for value in in_list:
        final_sum = final_sum + value
    return final_sum


def do_work(n):
    # create a random list of N integers
    my_list = gen_list(n)
    final_sum = sum_list(my_list)
    return final_sum



#  MULTI-PROCESING
# def multi_process(function_field='do_work', path_field='path_count', interval_field='interval_count'):
#     pool = mp.Pool(processes=mp.cpu_count())
#     chunks = 20
#     Q = int(path_field / chunks)
#     return np.hstack(pool.map(function_field, chunks * [(interval_field, Q)]))

#  TEST
if __name__ == '__main__':

    #  set assumptions


    #  start test
    data = pd.read_csv('data/lifetimeexample.csv', parse_dates=True)
    data['volatility'] = 0.15
    data['risk_free'] = 0

    #  create random forwards and strikes
    data['forward'] = np.random.uniform(5, 105, len(data))
    data['strike'] = np.random.uniform(1, 99, len(data))
    data['underlying'] = np.random.uniform(50, 200, len(data))

    print('\n***Creating data and checking fields***')
    data['date'] = make_dates(data, year_field='year', month_field='month', day_field='day')
    data['expiry'] = make_dates(data, year_field='Expiry year', month_field='Expiry month', day_field='Expiry day')
    data = data.drop(columns=['Date', 'month', 'year', 'day','Expiry', 'Expiry year', 'Expiry month', 'Expiry day', 'Yearlifetime', 'MinuteLifetime'])
    data['year lifetime'] = get_option_lifetimes(data, date_field='date', time_delta_unit='Y', expiry_field='expiry')
    data['min lifetime'] = get_option_lifetimes(data, date_field='date', time_delta_unit='m', expiry_field='expiry')
    data['modDate'] = get_eod_times(data, date_field='date', market_field='spx')

    print('\n***getting market expiry times***')
    data['spx'] = get_market_expiry_times(data, expiry_field='date', market_field='spx')
    data['es'] = get_market_expiry_times(data, expiry_field='date', market_field='es')

    print('\n***calculating calls and puts***')  # KEEP
    data['call'] = single_option_price(call_put=1, forward=102, strike=100, risk_free=0,
                                      lifetime=data['year lifetime'], volatility=0.15)
    data['put'] = single_option_price(call_put=-1, forward=102, strike=100, risk_free=0,
                                      lifetime=data['year lifetime'], volatility=0.15)

    print('\n***calculating IV***')
    data['iv'] = implied_volatility(data, call_put_field='call_put', price_field='call', forward_field='forward', strike_field='strike',
                       lifetime_field='year lifetime', risk_free_field='risk_free', vol_field='volatility', max_iter=1e5, tolerance=1e-6)



