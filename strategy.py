import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import itertools as it
import plotly.graph_objects as go

import plotly.express as px

from covel_directory_driver import covel_directory_driver

plt.style.use('seaborn')

# preserves individual markets but merges into single df horizontally
# will read in alphanumerically as well; contracts in 2000 2001 and 2002 are read and merged before 1975 -- 1999
# NO LONGER NEEDED AFTER CREATING/WRITING COMBINED DFs TO CSV IN data/combined_dfs LEAVE FOR REFERENCE MUST CREATE WAY FOR THIS TO HANDLE MULTIPLE
# DATA SOURCES e.g. REMOVE prep_covel and put in its place something descriptive of source .... or create a source driver ... if source_drive =
# ftp.cmeg: prep_ftp_cmeg(directory_name, filename)
def get_market_data(market_symbol, endswith_field='.txt'):
    directory = covel_directory_driver(market_symbol)
    list_of_files = [file for file in os.listdir(directory) if file.endswith(endswith_field)]
    df = pd.DataFrame()  # must remain or will receive reference before assignment
    list_of_dfs = [prep_covel(directory + filename) for filename in list_of_files]
    names = [file.replace('.txt', '') for file in list_of_files]
    for i, name in zip(list_of_dfs, names):  # <---- needs to not use merge?
        i = i.add_suffix('_' + name)
        df = pd.merge(df, i, right_index=True, left_index=True, how='outer')
    return df


# NO LONGER NEEDED AFTER CREATING/WRITING COMBINED DFs TO CSV IN data/combined_dfs LEAVE FOR REFERENCE
def prep_covel(relative_path):
    # prep data: step 1, process files with headers. Step 2, process files w/o headers and dates as ints.
    try:
        df = pd.read_csv(relative_path)
        df['Date'] = pd.to_datetime(df['Date'], format='%m/%d/%Y')
    except KeyError:
        # converters={'Date': lambda x: str(x)} prevents 3 leading zeros being dropped from 000101 (Jan 1, 2000)
        df = pd.read_csv(relative_path,
                         names=['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'OpenInt'], converters={'Date': lambda x: str(x)})
        df['Date'] = pd.to_datetime(df['Date'], format='%y%m%d')
    # adjust for date mapping issues: values 69–99 are mapped to 1969–1999, and values 0–68 are mapped to 2000–2068.
    df.loc[df['Date'].dt.year >= 2058, 'Date'] = df.loc[df['Date'].dt.year >= 2058, 'Date'] - np.timedelta64(100, 'Y')

    return df.set_index('Date')


def show_plot(input_data: object, market_name: object = str, field_name: object = str):
    fields = [i for i in input_data.columns if f'{field_name}_' in i]
    plt.figure(figsize=(14, 8))
    plt.plot(input_data[fields], linewidth=0.5)
    plt.title(f'{market_name} {field_name}')
    plt.legend([])
    return plt.show()


def save_plot(input_data, market_name: object = str, field_name: object = str, directory_name='', img_type='.png'):  # leaving directory name as empty string and img_type as .png will default save to pwd
    fields = [i for i in input_data.columns if f'{field_name}_' in i]
    plt.figure(figsize=(14, 8))
    plt.plot(input_data[fields], linewidth=0.5)
    plt.title(f'{market_name} {field_name}')
    plt.legend([])
    return plt.savefig(f'{directory_name}' + f'{market_name}' + f'{field_name}' + f'{img_type}')


# cannot be used until continuous contracts are made
def plotly_plot(input_data, market_name=str, field_name=str):
    fields = [i for i in input_data.columns if f'{field_name}_' in i]
    input_data['Date'] = input_data.index
    fig = px.line(input_data[fields], x='Date')
    return fig.write_html(f'{market_name}.html', auto_open=True)


# leave for reference DO NOT USE MULTIPLE TIMES, TAKES ~13 MINUTES TO PROCESS ALL MARKETS TO CSVs
# def all_markets_to_combined_dfs(main_directory_relative_path):
#     # this was done to automate testing of all, any date errors: errors will default to 1970-01-01 00:00:00 and be vertical on time series
#     list_of_markets = [file for file in os.listdir(f'{main_directory_relative_path}')]
#     bigg = [get_market_data(market) for market in list_of_markets]
#     print('starting')
#     for market, little in zip(list_of_markets, bigg):
#         little.to_csv(f"data/combined_dfs/{market}")


def fix_quotes_100x(input_data, price_limit_field=int):

    def direct_calc_100x(input_data, price_field): return (1 / input_data[price_field]) * 100

    input_data_highs = [i for i in input_data.columns if 'High_' in i]
    input_data[input_data[input_data_highs] > price_limit_field] = direct_calc_100x(input_data[input_data[input_data_highs] > price_limit_field],
                                                                                    input_data_highs)

    input_data_opens = [i for i in input_data.columns if 'Open_' in i]
    input_data[input_data[input_data_opens] > price_limit_field] = direct_calc_100x(input_data[input_data[input_data_opens] > price_limit_field],
                                                                                    input_data_opens)

    input_data_lows = [i for i in input_data.columns if 'Low_' in i]
    input_data[input_data[input_data_lows] > price_limit_field] = direct_calc_100x(input_data[input_data[input_data_lows] > price_limit_field],
                                                                                   input_data_lows)

    input_data_closes = [i for i in input_data.columns if 'Close_' in i]
    input_data[input_data[input_data_closes] > price_limit_field] = direct_calc_100x(input_data[input_data[input_data_closes] > price_limit_field],
                                                                                     input_data_closes)

    return input_data


def fix_quotes_norm(input_data, price_limit_field=int):

    def direct_calc_normal(input_data, price_field): return 1 / input_data[price_field]

    input_data_highs = [i for i in input_data.columns if 'High_' in i]
    input_data[input_data[input_data_highs] > price_limit_field] = direct_calc_normal(input_data[input_data[input_data_highs] >
                                                                                                 price_limit_field], input_data_highs)

    input_data_opens = [i for i in input_data.columns if 'Open_' in i]
    input_data[input_data[input_data_opens] > price_limit_field] = direct_calc_normal(input_data[input_data[input_data_opens] >
                                                                                                 price_limit_field], input_data_opens)

    input_data_lows = [i for i in input_data.columns if 'Low_' in i]
    input_data[input_data[input_data_lows] > price_limit_field] = direct_calc_normal(input_data[input_data[input_data_lows] >
                                                                                                price_limit_field], input_data_lows)

    input_data_closes = [i for i in input_data.columns if 'Close_' in i]
    input_data[input_data[input_data_closes] > price_limit_field] = direct_calc_normal(input_data[input_data[input_data_closes] >
                                                                                                  price_limit_field], input_data_closes)

    return input_data


def fix_open_interest(input_data):
    # had to implement to fix open interest on cd, chf, aud, gbp because I omitted _ from Open_ in fix_quotes_indirect_direct
    input_data_openint = [i for i in input_data.columns if 'OpenInt' in i]
    input_data[input_data_openint] = (1 / input_data[input_data_openint]) * 100
    return


def run_strategy(market_name):
    input_data = get_market_data(market_name)
    input_data_closes = [i for i in input_data.columns if 'Close_' in i] # <-- get rid of this, should be 1 close column

    def create_strategy(input_data, fast_ma, slow_ma, input_data_closes):
        input_data['fast_ma'] = input_data[input_data_closes].ewm(span=fast_ma)
        input_data['slow_ma'] = input_data[input_data_closes].ewm(span=slow_ma)
        input_data['Log_Ret'] = np.log(input_data[input_data_closes] / input_data[input_data_closes].shift(1))
        input_data['Position'] = np.where(input_data[fast_ma] > input_data[slow_ma], 1, -1)
        input_data['Returns'] = np.log(input_data[input_data_closes] / input_data[input_data_closes].shift(1)) # <-- get rid of input_data_closes, should be 1 close column
        input_data['Strategy'] = input_data['Position'].shift(1) * input_data['Returns']
        # trades = (input_data['Position'].diff().fillna(0) != 0) <-- keep for transaction costs
        # input_data['Strategy'] = np.where(trades, input_data['Strategy'] - self.tc, input_data['Strategy']) <-- keep for transaction costs
        input_data['CReturns'] = input_data['Returns'].cumsum().apply(np.exp)
        input_data['CStrategy'] = input_data['Strategy'].cumsum().apply(np.exp)
        return input_data[['CReturns', 'CStrategy']].iloc[-1]

    for fast_ma, slow_ma in it.product(range(12, 116, 1), range(26, 252, 1)):
        # print(SMA1, SMA2)
        res = create_strategy(input_data, fast_ma, slow_ma, input_data_closes)
        # print(res)
        results = results.append(pd.DataFrame({'Fast': fast_ma, 'Slow': slow_ma, 'CReturns': res['CReturns'],
                                               'CStrategy': res['CStrategy']}, index=[0]), ignore_index=True)

    annual_vol = input_data[['CReturns', 'CStrategy']].std() * 252 ** 0.5
    ema_pair = results['CStrategy'].idxmax()

    return f'The annual volatility is {annual_vol} and the EMA pair with the highest return is {ema_pair}'

