from strategy import *
import pandas as pd
import numpy as np

def make_continuous(market_name):
    directory = covel_directory_driver('british_pound')
    list_of_files = [file for file in os.listdir(directory) if file.endswith('.txt')]
    futures = [i for i in list_of_files if '00' in i or '01' in i or '02' in i]
    list_of_dfs = [prep_covel(directory + filename) for filename in futures]
    names = [file.replace('.txt', '') for file in futures]

    df = pd.DataFrame()  # must remain or will receive reference before assignment
    continuous = pd.DataFrame()

    count = 0
    while count < (len(futures) + 1):
        for contract, name in zip(list_of_dfs, names):

            if count == 0:
                combined = pd.merge(df, contract, right_index=True, left_index=True, how='outer')
            else:
                near = combined
                far = list_of_dfs[count + 1]
                near_name = near.add_suffix('_' + name)
                far_name = far.add_suffix('_' + name)
                combined = pd.merge(near, far, right_index=True, left_index=True, how='outer')
                high_openint = combined[f'OpenInt{near_name}'] > combined[f'OpenInt{far_name}']
                roll_date = high_openint[high_openint].index[0]
                continuous = pd.concat([near[near.index < roll_date],
                                        far[far.index >= roll_date]])
        count += 1

    return continuous


bp = make_continuous('british_pound')  # <--- time consuming because function is merging 1975-2002 yrs of daily price data
show_plot(bp, market_name='Pound', field_name='Close')
print('Data Head:',  bp.head(), sep='\n')
print('\n\nData Tail:', bp.tail(), sep='\n')
print('\n\nData Info:')
bp.info()  # Info does not like to be called in print line?
print('\n\nData Describe:', bp.describe(), sep='\n')
# start finding rolls dates
# open_ints = [i for i in bp.columns if 'OpenInt' in i]
# bp['roll_date'] = pd.Series([bp[i].idxmax() for i in open_ints]).fillna(0)
# bp['roll'] = np.where(bp['roll_date'] == bp.index, 1, 0)
